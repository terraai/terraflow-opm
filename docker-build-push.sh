#!/usr/bin/env bash

docker build -t $REPOSITORY .
if [ "$CIRCLE_BRANCH" == "master" ]; then
    echo "Building $CIRCLE_BRANCH branch and pushing to accont $AWS_ACCOUNT_ID in $AWS_DEFAULT_REGION region"
    docker tag $REPOSITORY:latest $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$REPOSITORY:latest
    docker push $AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com/$REPOSITORY:latest
fi