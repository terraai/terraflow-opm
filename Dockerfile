FROM 830543758886.dkr.ecr.us-east-2.amazonaws.com/terraflow-base-ubuntu:latest

ARG AIRFLOW_HOME=/usr/local/airflow

RUN apt-get update \
 && apt-get -y install software-properties-common

RUN apt-add-repository ppa:opm/ppa \
 && apt-get update

RUN apt-get -y install mpi-default-bin \
 && apt-get -y install libopm-simulators-bin

#COPY requirements.txt ${AIRFLOW_HOME}/requirements.txt
#RUN pip install -r ${AIRFLOW_HOME}/requirements.txt

# If sticking with cloudwatch then remove
EXPOSE 8793

USER airflow
WORKDIR ${AIRFLOW_HOME}
ENTRYPOINT ["/entrypoint.sh"]